# -*- coding: utf-8 -*-
"""
Created on Sun Apr  4 09:21:50 2021

@author: user
"""

import pandas as pd
import audio_file_representation as afr
import pickle

with open('data_representations.pickle', 'rb') as handle:
    loaded_structure = pickle.load(handle)

# %% isolate useful information
tmp_dict = {
    'actor_ID': [],
    'female': [],
    'mean_centroid': [],
    'std_centroid': [],
    'mean_bandwidth': [],
    'std_bandwidth': []
}
# information is appended to the already loaded structure
# shallow copy
for r in loaded_structure:
    tmp_dict['actor_ID'].append( r.actor_ID )
    tmp_dict['female'].append( r.female )
    tmp_dict['mean_centroid'].append( r.mean_centroid )
    tmp_dict['std_centroid'].append( r.std_centroid )
    tmp_dict['mean_bandwidth'].append( r.mean_bandwidth )
    tmp_dict['std_bandwidth'].append( r.std_bandwidth )

# %% create dataframe
df = pd.DataFrame( tmp_dict )

# and save
df.to_pickle('prepared_dataframe.pickle')